import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port: number = 3000

  // u need to call http://localhost:${port}/users/reload
  // to cache all users

  console.log(`[+] Application is running on port: ${port}`)
  await app.listen(port);
}
bootstrap();

