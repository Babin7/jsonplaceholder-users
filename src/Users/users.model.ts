import { ResponseI } from '../Interface/Response'

export class UserModal{
    res: ResponseI = {
        message: "",
        status: 0,
        isAccomplished: false,
        body: ""
    }
    constructor(public message, public status, public isAccomplished, public body?){}

    sendResponse(): ResponseI{
        this.res.isAccomplished = this.isAccomplished
        this.res.message = this.message
        this.res.status = this.status
        this.res.body = this.body
        
        return this.res
    }
}