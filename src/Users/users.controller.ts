import { Body, Controller, Delete, Get, Param, Patch, Query } from '@nestjs/common'
import axios from 'axios'
import { UserService } from './users.service'
import { ResponseI } from '../Interface/Response'
import { UserModal } from './users.model'
import { UsersI } from './users.interface'

@Controller("/users")
export class UserC{
    base_url: string = 'https://jsonplaceholder.typicode.com'
    headers: {} = {
        "Content-Type": "application/json"
    }

    constructor(private readonly userService: UserService){}

    @Get('/reload')
    async reloadUsers(): Promise<ResponseI>{
        const res: any = await axios.get(`${this.base_url}/users` , { headers: this.headers })
        const result: boolean = this.userService.filterUsers(res.data);
        
        if(result){
            return new UserModal("successfully reloaded all users" , 201, true)
                .sendResponse();       
        }   

        else{
            return new UserModal("internal problem while reloading users" , 400, false)
                .sendResponse(); 
        }
    }

    @Get(':id')
    async getUserByID(@Param() userID: any){
        const res: UsersI = this.userService.getUserByID(userID.id);
        if(res != null){
            return new UserModal("successfully fetch single users" , 201, true, res)
                .sendResponse();
        }
        else{
            return new UserModal("internal problem while fetching single user" , 400, false)
                .sendResponse();
        }
    }

    @Get()
    getAllUsers(): ResponseI{
        const result: UsersI[] = this.userService.getAllUser()

        if(result.length >= 1){
            return new UserModal("Here is list of all users data" , 201, true, result)
                .sendResponse();
        }
        else{
            return new UserModal("internal problem while accessing all users" , 400, false)
                .sendResponse(); 
        }
    }

    @Delete(':id')
    deleteUser(@Param() userId: any){
        const result: boolean = this.userService.deleteUser(userId.id)

        if(result){
            return new UserModal("Successfully deleted user" , 201, true)
                .sendResponse();
        }
        else{
            return new UserModal("Error while deleting user" , 400 , false)
                .sendResponse();
        }
    }

    @Patch(":id")
    updateUsername(@Param() param: any , @Query() name: any){
        const id = parseInt(param.id)
        const { username } = name

        const update_res: UsersI = this.userService.updateUsername(id, username)
        
        if(update_res != null){
            return new UserModal("Successfully updated user" , 201, true, update_res)
                .sendResponse();
        }
        else{
            return new UserModal("Error while deleting user" , 400 , false)
                .sendResponse();
        }
    }
}