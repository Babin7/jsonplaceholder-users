import { Module } from '@nestjs/common'
import { UserC } from './users.controller'
import { UserService } from './users.service'


@Module({
    imports: [],
    controllers: [UserC],
    providers: [UserService]
})

export class UsersModule{}