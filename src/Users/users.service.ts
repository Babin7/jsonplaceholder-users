import { Injectable } from '@nestjs/common'
import { UsersI, AddressI, CompanyI, GeoI } from './users.interface'

@Injectable()
export class UserService{
    all_users: UsersI[] = []

    constructor(){}

    filterUsers(res: any[]): boolean{
        res.map((user: any) => {
             const geo: GeoI = {
                lat: user.address.geo.lat,
                lng: user.address.geo.lng
            }


             const address: AddressI  = {
                street: user.address.street,
                suite: user.address.suite,
                city: user.address.city,
                zip: user.address.zipcode,
                geo: geo
            }

            const company: CompanyI = {
                name: user.company.name,
                catchPhrase: user.company.catchPhrase,
                bs: user.company.bs
            }
            
            const users: UsersI = {
                id: user.id,
                name: user.name,
                username: user.username,
                email: user.email,
                address: address,
                phone: user.phone,
                website: user.website,
                company
            }


            this.all_users.push(users)
        })

        return this.all_users.length > 1 ? true : false
    }

    getAllUser(): UsersI[]{
        return this.all_users
    }

    getUserByID(id: string): UsersI{
        if(this.all_users.length >= 1){
            const user: UsersI[] = this.all_users.filter((user: UsersI) => user.id.toString() === id.toString())
            return user[0];
        }

        else{
            return null
        }
    }


    deleteUser(id: string): boolean {
        if(this.all_users.length >= 1){
            try{
                const item = this.all_users.filter((user: UsersI) => user.id.toString() != id.toString())
                this.all_users = item;
                return true
            }
            catch(e){
                console.log(`[+] ${e.message} occurred while deleting user`)
                return false
            }
        }

        else{
            return false
        }
    }


    updateUsername(id: number, name: string) : UsersI{
        const valid_id: boolean = this.validateID(id)
        if(valid_id){
            try{
                const user: UsersI = this.getUserByID(id.toString())
                user.username = name
                return user
            }
            catch(e){
                console.log(`${e.message} while updating user`)
                return null
            }
        }
        else{
            return null
        }
    }


    private validateID(id: number): boolean{
        return this.all_users.map((user: UsersI) => user.id === id) ? true : false
    }
}