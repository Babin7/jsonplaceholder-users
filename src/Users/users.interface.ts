export interface UsersI{
    id: number
    name: string
    username: string
    email: string
    phone: string
    website: string
    address: AddressI
    company: CompanyI
}

export interface AddressI{
    street: string
    suite: string
    city: string
    zip: string
    geo: GeoI
}

export interface CompanyI{
    name: string
    catchPhrase: string
    bs: string
}

export interface GeoI{
    lat: string
    lng: string
}
